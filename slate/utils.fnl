(fn within [mx my x y w h]
  (and (> mx x) (<= mx (+ x w)) (> my y) (<= my (+ y h))))

{: within}
