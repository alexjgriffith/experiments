;; features to add
;; - panning
;; - expanding
;; - exit / open controls
;; - maximize option (full screen, no canvas)

(local love (require :love))
(local lg love.graphics)

(local _PATH (: ... :gsub "%.slab$" ""))
(local {: within} (require (.. _PATH :. :utils)))

(local slab {})

(local middle-mouse 3)
(local left-mouse 1)
(local right-mouse 2)

(fn slab.new [x y w h {:draw-callback draw-callback?
                       :mousepressed-callback mousepressed-callback?
                       :mousereleased-callback mousereleased-callback?
                       :mousemoved-callback mousemoved-callback?
                       :wheelmoved-callback wheelmoved-callback?
                       :unfocus-callback unfocus-callback?
                       :focus-callback focus-callback?
                       :init init?}]
  (local s
         (setmetatable
          {: x : y : w : h :header 30
           :ox 0 :oy 0
           :pox 0 :poy 0
           :scale 1
           :max-w w
           :max-h h
           :mx 0 :my 0
           :canvas (lg.newCanvas w h)
           :quad (lg.newQuad 0 0 w h w h)
           :draw-callback (or draw-callback? (fn [_self]))
           :mousepressed-callback (or mousepressed-callback? (fn [_self]))
           :mousereleased-callback (or mousereleased-callback? (fn [_self]))
           :mousemoved-callback (or mousemoved-callback? (fn [_self]))
           :wheelmoved-callback (or wheelmoved-callback? (fn [_self]))
           :unfocus-callback (or unfocus-callback? (fn [_self]))
           :focus-callback (or focus-callback? (fn [_self]))
           }
          {:__index slab}))
  (when init? (init? s))
  s)

(fn slab.focus [self]
  (self:focus-callback))

(fn slab.unfocus [self]
  (self:unfocus-callback))

(fn slab.within [self mx my]
  (within mx my self.x self.y self.w self.h))

(fn slab.within-header [self mx my]
  (when self.header
        (within mx my self.x (- self.y self.header) self.w self.h)))

(fn slab.draw [self]
  (lg.push :all)
  (lg.setCanvas self.canvas)
  (when self.draw-callback (self:draw-callback))
  (lg.setCanvas)
  (lg.setColor 0.2 0.2 0.2 1)
  (lg.rectangle :fill self.x (- self.y self.header) self.w self.header)
  (lg.setColor 1 1 1 1)
  (lg.draw self.canvas self.quad self.x self.y)
  (lg.pop))

(fn begin-pan [self mx my]
  (tset self :pan true)
  (tset self :pox self.ox)
  (tset self :poy self.oy)
  (tset self :mx mx)
  (tset self :my my))

(fn update-pan [self mx my]
  (when self.pan
        (tset self :ox (-> (- mx) (+ self.mx) (+ self.pox)
                           (math.max 0) (math.min (- self.max-w self.w))))
        (tset self :oy (-> (- my) (+ self.my) (+ self.poy)
                           (math.max 0) (math.min (- self.max-h self.h))))))

(fn end-pan [self mx my]
  (tset self :pan false))

(fn slab.mousepressed [self x y button mx my]
  (match button middle-mouse (begin-pan self mx my))
  (self:mousepressed-callback x y mx my))

(fn slab.mousemoved [self x y mx my]
  (update-pan self mx my)
  (self:mousemoved-callback x y mx my))

(fn slab.mousereleased [self x y button mx my]
  (match button middle-mouse (end-pan self mx my))
  (self:mousereleased-callback x y mx my))

(fn slab.wheelmoved [self x y]
  (pp [self.scale y (/ y 10)])
  (set self.scale (* self.scale (+ 1 (/ y 10))))
  ;;(update-pan self mx my)
  (self:wheelmoved-callback x y))

slab
