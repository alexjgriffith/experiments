(local love (require :love))
(local lg love.graphics)
(local _PATH (: ... :gsub "%.grid$" ""))

(local {: within} (require (.. _PATH :. :utils)))

(local grid {})

(fn grid.init [self]
  (local grid [])
  (local grid-w 7)
  (local grid-h 5)
  (local size (* 16 4))
  (set self.w (* size grid-w))
  (set self.h (* size grid-h))
  (self.quad:setViewport 0 0 self.w self.h)
  (for [i 1 grid-w]
    (local row [])
    (for [j 1 grid-h]
      (table.insert row {:colour [0.5 0.5 0.5 1]}))
    (table.insert grid row))
  (set self.size size)
  (set self.grid grid)
  (set self.grid-w grid-w)
  (set self.grid-h grid-h)
  (set self.over nil)
  (set self.rect-colour [0.5 0.5 0.5 1]))

(fn grid.draw [self]
  (lg.clear 0.8 0.9 0.9 1)
  (local size self.size)
  (for [i 1 self.grid-w]
    (for [j 1 self.grid-h]
      (lg.setColor (. self.grid i j :colour))
      (lg.rectangle :fill (* (- i 1) size) (* (- j 1) size) size size)
      (lg.setColor 0 0 0 1)
      (lg.rectangle :line (+ (* (- i 1) size) 10) (+ (* (- j 1) size) 10)
          (- size 20) (- size 20))
      ))
  )

(fn grid.mousemoved [self x y mx my]
  (let [{: size : grid-w : grid-h} self]
    (if (within x y 0 0 (- (* size  grid-w) 1) (- (* size  grid-h) 1))
      (let [i (-> x (/ size) (math.floor) (+ 1))
            j (-> y (/ size) (math.floor) (+ 1))]
        (tset self.grid i j :colour [0 0.5 0.5 1])
        (when (and self.over (not (and (= (. self.over 1) i) (= (. self.over 2) j))))
              (let [[i j] self.over]
                (tset self.grid i j :colour [0.5 0.5 0.5 1])))
        (tset self :over [i j])
        )
      (do
        (when self.over
              (let [[i j] self.over]
                (tset self.grid i j :colour [0.5 0.5 0.5 1])))
        (tset self :over nil))
      )    
    )
  )

(fn grid.unfocus [self]
  (when self.over
              (let [[i j] self.over]
                (tset self.grid i j :colour [0.5 0.5 0.5 1])))
  )

grid
