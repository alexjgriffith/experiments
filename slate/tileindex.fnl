(local love (require :love))
(local lg love.graphics)
(local _PATH (: ... :gsub "%.tileindex$" ""))

(pp _PATH)

(local {: within} (require (.. _PATH :. :utils)))

(local tileindex {})

(fn tileindex.init [self]
  (local lg love.graphics)
  (tset self :tileindex [])
  (tset self :image (lg.newImage "tiles.png"))
  (tset self :scale 4)
  (tset self :tile-size 16)
  (tset self :tileindex-w (-> (self.image:getWidth)
                         (/ self.tile-size)
                         (math.floor)))
  (tset self :tileindex-h (-> (self.image:getHeight)
                         (/ self.tile-size)
                         (math.floor)))
  (tset self :scale 4)
  (local size (* self.tile-size self.scale))
  (set self.size size)
  (set self.max-w (* size self.tileindex-w))
  (set self.max-h (* size self.tileindex-h))
  ;; (self.quad:setViewport 0 0 self.w self.h)
  (for [i 1 self.tileindex-w]
    (local row [])
    (for [j 1 self.tileindex-h]
      (table.insert row {:colour [0.5 0.5 0.5 0]}))
    (table.insert self.tileindex row))
  (set self.over nil)
  (set self.rect-colour [0.5 0.5 0.5 0])
  (set self.hover-colour [0.5 0.5 0.5 0.5]))

(fn tileindex.draw [self]
  (lg.clear 1 1 1 1)
  (lg.push :all)
  (lg.setColor 1 1 1 1)
  (lg.translate (- self.ox) (- self.oy))
  (lg.scale self.scale)  
  (lg.draw self.image)
  (lg.pop)
  (local size self.size)
  (for [i 1 self.tileindex-w]
    (for [j 1 self.tileindex-h]
      (lg.setColor (. self.tileindex i j :colour))
      ;; (lg.rectangle :fill (* (- i 1) size) (* (- j 1) size) size size)
      ;;(lg.setColor 0 0 0 1)
      (lg.setColor (. self.tileindex i j :colour))
      (lg.rectangle :line (+ (* (- i 1) size) 10) (+ (* (- j 1) size) 10)
          (- size 20) (- size 20))
      ))
  )

(fn tileindex.mousemoved [self x y mx my]
  (let [{: size : tileindex-w : tileindex-h} self]
    (if (within x y 0 0 (- (* size  tileindex-w) 1) (- (* size  tileindex-h) 1))
      (let [i (-> x (/ size) (math.floor) (+ 1))
            j (-> y (/ size) (math.floor) (+ 1))]
        (tset self.tileindex i j :colour self.hover-colour)
        (when (and self.over (not (and (= (. self.over 1) i) (= (. self.over 2) j))))
              (let [[i j] self.over]
                (tset self.tileindex i j :colour self.rect-colour)))
        (tset self :over [i j])
        )
      (do
        (when self.over
              (let [[i j] self.over]
                (tset self.tileindex i j :colour self.rect-colour)))
        (tset self :over nil))
      )    
    )
  )

(fn tileindex.unfocus [self]
  (when self.over
              (let [[i j] self.over]
                (tset self.tileindex i j :colour self.rect-colour)))
  )

tileindex
