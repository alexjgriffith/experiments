;; Features
;; fixed-bottom elements?
;; cascading ui signals (prevent click through if two seperate UIs are on the screen)
;; keypress capture / keyboard state recording (maybe easier to use isDown?)
;; spacial hash rather than table for element storage for better indexing?


(local ui {})

(fn ui.new [] (setmetatable {:next-id 1 :elements []} {:__index ui}))

(fn ui.add [self what ...]
  (let [id self.next-id]
    (table.insert self.elements (what.new ...))
    (set self.next-id (+ self.next-id 1))))

(fn ui.draw [self]
  (each [_ element (ipairs self.elements)]
    (element:draw)))

(fn ui.get-target-element [self x y]
  (when (and x y (not self.target-element))
    (set self.target-element
         (accumulate [highest nil index element (ipairs self.elements)]
      (if (or (element:within x y)
              (element:within-header x y))
          index
          highest))))
  (values self.target-element
          (when self.target-element (. self.elements self.target-element))))

(fn ui.clear-target-element [self]
  (set self.previous-element self.target-element)
  (set self.target-element nil))

(fn ui.mousepressed [self x y button]
  (let [(index element) (ui.get-target-element self x y)]
    (when element
      ;; reorder elements
      (when (> (# self.elements) 1)
        (for [i (+ index 1) (# self.elements)]
          (tset self.elements (- i 1) (. self.elements i)))
        (tset self.elements (# self.elements) element))
      (if (element:within x y)
          (do
            (tset self :element-pressed element)
            (element:mousepressed
                   (- x element.x)
                   (- y element.y)
                   button x y))
          (element:within-header x y)
          (set ui.header-selected {: element : x : y
                                   :ex element.x :ey element.y})))))

(fn ui.mousereleased [self x y button]
  (when self.element-pressed
        (self.element-pressed:mousereleased
              (- x self.element-pressed.x)
              (- y self.element-pressed.y)
              button x y))
  (set ui.header-selected false)
  (set self.element-pressed false))  

(fn ui.focus-unfocus [self element]
  (when self.previous-element
        (let [previous-element (. self.elements self.previous-element)]
          (if (not element) (previous-element:unfocus)
              (~= element self.previous-element) (previous-element:unfocus))))
  (when element (element:focus)))

(fn ui.wheelmoved [self x y]
  (local (mx my) (love.mouse.getPosition))
  (let [(_ element) (ui.get-target-element self mx my)]
  (if
   element (element:wheelmoved x y))))

(fn ui.mousemoved [self x y button]
  (if
   ui.header-selected
   (do (set ui.header-selected.element.x
            (+ ui.header-selected.ex (- x ui.header-selected.x)))
       (set ui.header-selected.element.y
            (+ ui.header-selected.ey (- y ui.header-selected.y))))
   
   ui.element-pressed (ui.element-selected:mousemoved)
   
   (let [index (ui.get-target-element self x y)
         element (. self.elements index)]
     (ui.focus-unfocus self element)
     (when element
       (if (element:within x y) (element:mousemoved
                                         (- x element.x)
                                         (- y element.y)
                                         x
                                         y)
           (element:within-header x y) :header-highlight
           
           ))
     )
   )
  )

ui
