love.conf = function(t)
   t.gammacorrect = false
   t.title, t.identity = "slate", "slate"
   t.modules.joystick = false
   t.modules.physics = false
   t.window.width = 1920-- 720 * 21 / 9 -- 1280 -- 1920
   t.window.height = 1080 -- 720 -- 1080
   t.window.vsync = true
   t.window.resizable = false
   t.window.fullscreen = false
   t.version = "11.3"
end
