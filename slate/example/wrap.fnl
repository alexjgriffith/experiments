(local love (require :love))

(love.graphics.setDefaultFilter :nearest :nearest)

(local ui (require :slate.ui))
(local slab (require :slate.slab))
(local grid (require :slate.grid))
(local tileindex (require :slate.tileindex))

(local my-ui (ui.new))

(set _G._ui my-ui)

(my-ui:add slab 50 50 500 500 {:draw-callback grid.draw
                               :init grid.init
                               :unfocus-callback grid.unfocus
                               :mousemoved-callback grid.mousemoved})


(my-ui:add slab 50 50 500 500 {:draw-callback grid.draw
                               :init grid.init
                               :unfocus-callback grid.unfocus
                               :mousemoved-callback grid.mousemoved})


(my-ui:add slab 500 500 200 200 {:draw-callback tileindex.draw
                                 :init tileindex.init
                                 :unfocus-callback tileindex.unfocus
                                 :mousemoved-callback tileindex.mousemoved})

(my-ui:add slab 50 50 500 500 {:draw-callback (fn [self] (love.graphics.clear 1 0.2 0.2 1))})

(fn love.load [])

(fn love.draw []
  (local lg love.graphics)
  (lg.clear 1 1 1 1)
  (let [(index element) (ui.get-target-element my-ui nil nil)]
    (when index
      (local message (string.format "element %s" index))
      (lg.setColor 0 0 0 1)
      (lg.print message 10 10)))
  (my-ui:draw)
  (my-ui:clear-target-element))

(fn love.mousemoved [x y dx dy]
  (my-ui:mousemoved x y))

(fn love.mousepressed [x y button]
  (my-ui:mousepressed x y button))

(fn love.mousereleased [x y button]
  (my-ui:mousereleased x y button))

(fn love.wheelmoved [x y ]
  (my-ui:wheelmoved x y))


(fn love.update [dt]
  (local (mx my) (love.mouse.getPosition))
  (ui.get-target-element my-ui mx my)
  )
