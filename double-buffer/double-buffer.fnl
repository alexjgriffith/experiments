;; Title:Love Double Buffer
;; Description:A simple buffer that can be cut and paste into.
;; Author:AlexJGriffith
;; Email:griffitaj@gmail.com
;; Version:0.1

;;; Example:
;; (local double-buffer (require :double-buffer))
;; 
;; (fn love.load []
;;   (love.keyboard.setKeyRepeat true)
;;   (let [font (love.graphics.newFont :inconsolata.otf 24)]
;;     (set _G.buffer-1 (double-buffer.new font []))))

;; (fn love.draw []
;;   (_G.buffer-1:draw))

;; (fn love.update [dt])

;; (fn love.keypressed [key scancode is-repeat]
;;   (_G.buffer-1:keypressed key _G.buffer-1.default-bindings))

;; (fn love.keyreleased [key scancode])

;; (fn love.textinput [key]
;;   (_G.buffer-1:textinput key _G.buffer-1.default-bindings))


;; (define-key fennel-mode-map (kbd "C-c C-z") (lambda () (interactive) (shell-command "kill $(ps aux | grep love-fennel | head -1 | awk '{print $2}')") (fennel-repl "love-fennel")))

(local double-buffer {})

(fn double-buffer.new [font styles]
  (local lg love.graphics)
  (fn draw-background [] (lg.clear 1 1 1 1))
  (fn draw-highlight [_ w h midway] (lg.setColor 0.9 0.9 0.9 1) (lg.rectangle :fill 0 0 w h))
  (fn draw-char [[char] w h midway]
    (if midway (lg.setColor 1 1 1 1) (lg.setColor 0 0 0 1))
    (lg.print char)
    ;; (lg.print "*")
    ;; (lg.line 0 h w h)
    ;; (lg.rectangle :line 0 0 w h)
    )
  (fn draw-mid [_ w h _]
    (lg.setColor 0 0 0 1)
    (lg.rectangle :fill 0 0 w h))
  (setmetatable {:left [] :right [] : font  : styles :chord-key false :mark false
                 : draw-background : draw-highlight : draw-char : draw-mid}
                {:__index double-buffer}))
  
(fn double-buffer.insert [{: left : font} key]  
  (table.insert left [key (font:getWidth key) (font:getHeight key) 0]))

(fn double-buffer.delete [{: left}] (table.remove left (# left)))

(fn double-buffer.move-left [{: right : left}]
  (let [index (# left)]
    (table.insert right (. left index))
    (table.remove left index)
    ;; always return the value at current point, current position and
    ;; positions moved
    (values (. left (# left)) (# left) 1)))

(fn double-buffer.move-left-n [db n]
  (for [i 1 n] (double-buffer.move-left db)))

(fn double-buffer.move-right [{: right : left}]
  (let [index (# right)]
    (table.insert left (. right index))
    (table.remove right index)
    (values (. left (# left)) (# left) 1)))

(fn double-buffer.move-right-n [db n]
  (for [i 1 n] (double-buffer.move-right db)))

(fn double-buffer.move-up [db]
  (let [previous-point (# db.left)
        (_ _ column) (double-buffer.move-line-start db)]
    (if (~= 0 (# db.left))
        (do (double-buffer.move-left db)
            (let [(_ _ column2) (double-buffer.move-line-start db)]
              (pp [column column2])
              (when (> (math.min column column2) 0)
                (double-buffer.move-right-n db (math.min column column2)))))
        (double-buffer.goto-point db previous-point)))
  (values (. db.left (# db.left)) (# db.left) 1))

(fn double-buffer.move-down [db]
  (let [previous-point (# db.left)
        (_ _ column) (double-buffer.move-line-start db)
        (_ _ _column) (double-buffer.move-line-end db)]
    (if (~= 0 (# db.right))
        (do (double-buffer.move-right db)
            (let [;; (_ _ column2) (double-buffer.move-line-start db)
                  (_ _ column2) (double-buffer.move-line-end db)
                  (_ _ _column2) (double-buffer.move-line-start db)]
              (pp [column column2])
              (when (> (math.min column column2) 0)
                (double-buffer.move-right-n db (math.min column column2)))))
        (double-buffer.goto-point db previous-point)))
  (values (. db.left (# db.left)) (# db.left) 1))

(fn double-buffer.move-start [{: right : left}]
  (let [len (# left)]
    (for [i 1 len]
      (let [j (+ 1 (- len i))]
        (table.insert right (. left j))
        (table.remove left j)))
    (values (. left (# left)) (# left) len)))

(fn double-buffer.move-end [{: right : left}]
  (let [len (# right)]
    (for [i 1 len]
      (let [j (+ 1 (- len i))]
        (table.insert left (. right j))
        (table.remove right j)))
    (values (. left (# left)) (# left) len)))

(fn double-buffer.move-line-start [{: right : left}]
  (let [len (# left)]
    (var char (. left (# left)))
    (var position-change 0)
    (while (and (> (# left) 0) char (~= (. char 1) "\n"))
      (set position-change (+ position-change 1))      
      (let [i position-change
            j (+ 1 (- len i))]
        (table.insert right (. left j))
        (table.remove left j))
      (set char (. left (# left))))
    (values (. left (# left)) (# left) position-change)))

(fn double-buffer.move-line-end [{: right : left}]
  (let [len (# right)]
    (var char (. right (# right)))
    (var position-change 0)
    (while (and (> (# right) 0) char (~= (. char 1) "\n"))
      (set position-change (+ position-change 1))      
      (let [i position-change
            j (+ 1 (- len i))]
        (table.insert left (. right j))
        (table.remove right j))
      (set char (. right (# right))))
    (values (. left (# left)) (# left) position-change)))

(fn double-buffer.goto-point [db point]
  (let [mid (# db.left)
        len (+ mid (# db.right))]
    (if (<= point 0) (double-buffer.move-start db)
        (>= point len) (double-buffer.move-end db)
        (< point mid) (double-buffer.move-left-n db (- mid point))
        (> point mid) (double-buffer.move-right-n db (- point mid))
        (= point mid) (values (. db.left mid) mid 0))))

(fn double-buffer.within-marks [{: mark : left} point]
  (let [mid (# left)]
    (if (and mark (< mark mid)) (and (>= point mark) (< point mid))
        (and mark (> mark mid)) (and (> point mid) (<= point mark))
        false)))

(fn double-buffer.kill-region [{: mark : left &as db}]
  (let [mid (# left)]
    (if (and mark (< mark mid))
        (do (double-buffer.goto-point db mid)
            (set db.mark false)
            (for [_i 1 (- mid mark)]
              (double-buffer.delete db)))
        (and mark (> mark mid))
        (do (double-buffer.goto-point db mark)
            (set db.mark false)
            (for [_i 1 (- mark mid)]
              (double-buffer.delete db)))
        (double-buffer.delete db))))

(fn double-buffer.copy-region [{: mark : left : right &as db}]
  (fn sub [buffer start end]
    (var str "")
    (for [i start end]
      (when (. buffer i)
        (set str (.. str (. buffer i 1)))))
    str)
  (let [mid (# left)
        len (- mark mid)
        end (# right)
        substring (if (and mark (< mark mid)) (sub left mark mid)
                      (and mark (> mark mid))
                      (do (-> (sub right (- end len) end)
                              (string.reverse)))
                      "")]
    (set db.mark false)
    (love.system.setClipboardText substring)))

(fn double-buffer.cut-region [{: mark : left : right &as db}]
  (let [mark db.mark]
    (double-buffer.copy-region db)
    (set db.mark mark)
    (double-buffer.kill-region db)))

(fn double-buffer.paste [{: mark : left : right &as db}]
  ;; Sample utf8 symbol:😊
  (when mark (double-buffer.kill-region db))
  (let [utf8 (require :utf8)
        text (love.system.getClipboardText)]
    (var i 1)
    (var start 1)
    (var end (utf8.offset text i))
    (while end
      (double-buffer.insert db (string.sub text start (- end 1)))
      (set start end)
      (set i (+ i 1))
      (set end (utf8.offset text i)))))

(fn double-buffer.set-mark [db]
  (set db.mark (if db.mark false (# db.left))))

(fn double-buffer.apply-backwards [{: right : left &as db} func]
  (var point (# left))
  (for [j 1 (# left)]
    (local i (+ 1 (- (# left) j)))
    (local (success key) (func db (. left i) point))
    (when success (lua "return key"))
    (set point (- point 1))))

(fn double-buffer.apply-forwards [{: right : left &as db} func]
  (var point (# right))
  (for [j 1 (# right)]
    (local i (+ 1 (- (# right) j)))
    (local (success key) (func db (. left i) point))
    (when success (lua "return key"))
    (set point (- point 1))))

(fn double-buffer.apply [{: right : left &as db} func mid-func]
  (var point 0)
  (for [i 1 (# left)]
    (func db (. left i) point)
    (set point (+ point 1)))
  (when mid-func
    (mid-func db (. right 1) point))
  (when (> (# right) 0)
    (for [j 1 (# right)]
      (local i (+ 1 (- (# right) j)))
      (func db (. right i) point)
      (set point (+ point 1)))))

(fn double-buffer.draw [{: right : left : font &as db}]
  (local lg love.graphics)
  (db.draw-background)
  (lg.setFont font)
  (var row-width 0)
  (var midway false)
  (local h (font:getHeight left))
  (fn func [db element point]
    (let [[char w] element]
      (match char 
        "\n"  (do
                (lg.translate (- row-width) h)
                (set row-width 0))
        _ (do
            (when (double-buffer.within-marks db point)
              (db.draw-highlight element w h midway))
            (db.draw-char element w h midway)
            (set row-width (+ row-width w))
            (lg.translate w 0))))
    (when midway (set midway false)))
  (fn mid-func [db element point]
    (local w (font:getWidth " "))
    (db.draw-mid element w h midway)
    (set midway true))
  (double-buffer.apply db func mid-func))

(fn double-buffer.encode-chord [str ctrl meta shift]
  (.. (if ctrl "C-" "") (if meta "M-" "") (if shift "S-" "") str))

(fn double-buffer.keypressed [db key bindings]
  (fn binding-call [key bindings]
    (let [isDown love.keyboard.isDown
          chord (double-buffer.encode-chord
                             key
                             (isDown "lctrl" "rctrl")
                             (isDown "lalt" "ralt")
                             (isDown "lshift" "rshift"))]
      (values (. bindings chord) chord)))
  (let [bc (binding-call key bindings)]
    (if bc
        (do (set db.chord-key true) (bc db key))
        (set db.chord-key false))))

(fn double-buffer.textinput [db key bindings]
  (when (not db.chord-key)
    (when db.mark (double-buffer.kill-region db))
    (double-buffer.insert db key bindings)))

(set double-buffer.emacs-bindings
     {:backspace (fn [db key] (db:kill-region))
      :return (fn [db key] (db:insert "\n"))
      :left (fn [db key] (db:move-left))
      :right (fn [db key] (db:move-right))
      :up (fn [db key] (db:move-up))
      :down (fn [db key] (db:move-down))
      "C-a" (fn [db key] (db:move-line-start))
      "C-e" (fn [db key] (db:move-line-end))
      "M-S-," (fn [db key] (db:move-start))
      "M-S-." (fn [db key] (db:move-end))
      "C-space" (fn [db key] (db:set-mark))
      "M-w" (fn [db key] (db:copy-region))
      "C-w" (fn [db key] (db:cut-region))
      "C-y" (fn [db key] (db:paste))
      })

(set double-buffer.simple-bindings
     {:backspace (fn [db key] (db:kill-region))
      :left (fn [db key] (db:move-left))
      :right (fn [db key] (db:move-right))
      "C-a" (fn [db key] (db:move-line-start))
      "C-e" (fn [db key] (db:move-line-end))
      "C-space" (fn [db key] (db:set-mark))
      "M-w" (fn [db key] (db:copy-region))
      "C-w" (fn [db key] (db:cut-region))
      "C-y" (fn [db key] (db:paste))
      })


(set double-buffer.default-bindings
     {:backspace (fn [db key] (db:kill-region))
      :return (fn [db key] (db:insert "\n"))
      :left (fn [db key] (db:move-left))
      :right (fn [db key] (db:move-right))
      :up (fn [db key] (db:move-up))
      :down (fn [db key] (db:move-down))
      "home" (fn [db key] (db:move-line-start))
      "end" (fn [db key] (db:move-line-end))
      "C-space" (fn [db key] (db:set-mark))
      "C-c" (fn [db key] (db:copy-region))
      "C-x" (fn [db key] (db:cut-region))
      "C-v" (fn [db key] (db:paste))
      })

double-buffer
