(local double-buffer (require :double-buffer))

(local buffers {})

(local font (love.graphics.newFont :inconsolata.otf 24))

(local lg love.graphics)

(fn password-active-draw [[char] w h midway]
  (if midway (lg.setColor 1 1 1 1) (lg.setColor 0 0 0 1))
  (lg.print "*"))
(fn password-inactive-draw [[char] w h midway]
  (lg.setColor 0 0 0 1)
  (lg.print "*"))
(fn username-active-draw [[char] w h midway]
      (if midway (lg.setColor 1 1 1 1) (lg.setColor 0 0 0 1))
      (lg.print char))
(fn username-inactive-draw [[char] w h midway]
      (lg.setColor 0 0 0 1)
      (lg.print char))    
(fn mid-active-draw [_ w h midway]
      (lg.setColor 0 0 0 1)
      (lg.rectangle :fill 0 0 w h))
(fn mid-inactive-draw [_ w h midway])

(fn love.load []
  (local (w h) (love.window.getMode))
  (love.keyboard.setKeyRepeat true)
  (let [font (love.graphics.newFont :inconsolata.otf 24)]
    (tset buffers :username {:buffer (double-buffer.new font [])
                             :canvas (love.graphics.newCanvas 300 30)
                             :name :username
                             :bindings double-buffer.simple-bindings
                             :x 10 :y 40 :w 300 :h 30})
    (tset buffers :password {:buffer (double-buffer.new font [])
                             :canvas (love.graphics.newCanvas 300 30)
                             :bindings double-buffer.simple-bindings
                             :name :password
                             :x 10 :y 110 :w 300 :h 30})

    (tset buffers :comments {:buffer (double-buffer.new font [])
                             :canvas (love.graphics.newCanvas (- w 20) (- h 190))
                             :bindings double-buffer.default-bindings
                             :name :comments
                             :x 10 :y 180 :w (- w 20) :h (- h 190)})
    (set buffers.password.buffer.draw-char password-inactive-draw)
    (set buffers.password.buffer.draw-mid mid-inactive-draw)
    (set buffers.comments.buffer.draw-char username-inactive-draw)
    (set buffers.comments.buffer.draw-mid mid-inactive-draw)    
    (set buffers.username.buffer.draw-char username-inactive-draw)
    (set buffers.username.buffer.draw-mid mid-inactive-draw)  
    ))

(fn in-buffer [buffers mx my]
  (var current-buffer nil)
  (each [_ {: x : y : w : h &as buffer} (pairs buffers)]
    (when (and (> mx x) (< mx (+ w x)) (> my y) (< my (+ h y)))
      (set current-buffer buffer)))
  current-buffer)

(fn love.draw []
  (local lg love.graphics)
  (love.graphics.clear 0.7 0.9 0.9 1)
  
  (lg.setFont font)
  (lg.setColor 0 0 0 1)
  (lg.print "Username:" 10 10)
  (lg.print "Password:" 10 80)
  (lg.print "Comments:" 10 150)
  (lg.setColor 1 1 1 1)
  (each [buffername {: buffer : canvas : x : y} (pairs buffers)]
    (love.graphics.push :all)
    (love.graphics.push :all)
    (love.graphics.setCanvas canvas)
    (love.graphics.clear 1 1 1 1)
    (buffer:draw)
    (love.graphics.pop)
    (love.graphics.setCanvas)
    (love.graphics.translate x y)
    (love.graphics.draw canvas)
    (love.graphics.pop)
    )
  )

(fn love.update [dt])

(fn love.mousemoved [x y]
  (local current-buffer (in-buffer buffers x y))
  (set buffers.password.buffer.draw-char password-inactive-draw)
  (set buffers.password.buffer.draw-mid mid-inactive-draw)
  (set buffers.username.buffer.draw-char username-inactive-draw)
  (set buffers.username.buffer.draw-mid mid-inactive-draw)
  (set buffers.comments.buffer.draw-char username-inactive-draw)
  (set buffers.comments.buffer.draw-mid mid-inactive-draw)    
  
  (when current-buffer
    (set current-buffer.buffer.draw-mid mid-active-draw)
    (match current-buffer.name
           :password (set current-buffer.buffer.draw-char password-active-draw)
            :username (set current-buffer.buffer.draw-char username-active-draw)
             :comments (set current-buffer.buffer.draw-char username-active-draw)
             )
    )
  )

(fn love.keypressed [key scancode is-repeat]
  (local (x y) (love.mouse.getPosition))
  (local current-buffer (in-buffer buffers x y))
  (when current-buffer
    (current-buffer.buffer:keypressed key current-buffer.bindings)))

(fn love.keyreleased [key scancode])

(fn love.textinput [key]
  (local (x y) (love.mouse.getPosition))
  (local current-buffer (in-buffer buffers x y))
  (when current-buffer
    (current-buffer.buffer:textinput key current-buffer.bindings)))
