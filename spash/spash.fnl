;; spash.fnl - The Simple No Garbage Spatial Hash
;; MIT LICENCE

;; Purpose: A UI use first spacial hash
;; Objectives: Branchless, No Garbage Created, Z ordering 

;; This Spatial hash is designed to be cleared and built each
;; frame without creating memory churn. It does not support
;; updating object locations or removing individual objects.

(local spash {:version :0.1.0})
(local spash-mt {:__index spash})

(macro trace [x ...] (when false `(print (string.format ,x ,...))))

(fn xy->index [x y] (+ x (* y 1e7)))

(fn add-tab [pool p-pool obj x y w h ?z]
  (let [id (+ (# pool) 1)
        tobj (or (. p-pool (# p-pool)) {})] ;; tobj_table created
    (tset tobj :obj obj)
    (tset tobj :x x)
    (tset tobj :y y)
    (tset tobj :w w)
    (tset tobj :h h)
    ;; if z is not provided it is set in cell
    (tset tobj :z ?z) 
    (tset tobj :id id)
    (table.remove p-pool (# p-pool))
    (tset pool id tobj)
    tobj))

(fn drop [tobj]
  (let [id tobj.id]    
    (tset tobj :obj nil) ;; tobj_table "cleared for reuse"
    ;; obj is set to nil so we don't hold on to
    ;; data owned by another program creating a memory
    ;; leak
    (values tobj.x tobj.y tobj.w tobj.h)))

(fn get-cell-range [cell-size x y w h]  
  (let [floor math.floor
        x1 (-> x (/ cell-size) (floor))
        x2 (-> x (+ w) (/ cell-size) (floor))
        y1 (-> y (/ cell-size) (floor))
        y2 (-> y (+ h) (/ cell-size) (floor))]
  (values x1 x2 y1 y2)))

(fn apply-to-cells [x1 x2 y1 y2 fun ...]
  (for [i x1 x2]
      (for [j y1 y2]
        (let [index (xy->index i j)]
          (fun index ...)))))

(fn insert-into-cell [index tab tobj]
  (let [cell (or (. tab index) [])] ;; cell_table created
    (tset tobj :z (or tobj.z (+ 1 (# cell))))
    (table.insert cell tobj)
    (tset tab index cell)))

(lambda spash.add [{: pool : p-pool : tab : cell-size} obj x y w h ?z]
  "Add an object to spash with rect x y w h.
Optionally, providing a ?z value will replace the order added based
z value assigned by the spash. If you're adding 100 objects and
you want one to always be on top give it a ?z value of 101.
An object can be any lua value or table. This value will be removed
from the spash on clear."
  (let [tobj (add-tab pool p-pool obj x y w h ?z)
        (x1 x2 y1 y2) (get-cell-range cell-size x y w h)]
    (trace "Adding to cells in: x1:%s, x2:%s, y1:%s, y2:%s, tobj:%s"
           x1 x2 y1 y2 (match (type tobj) :table :table :userdata :userdata _ tobj))
    (apply-to-cells x1 x2 y1 y2 insert-into-cell tab tobj)))

(fn clear-cell [index tab]
  (let [cell (. tab index)]
    (each [key _ (ipairs cell)]
      (tset cell key nil)))) ;; cell_table "cleared for reuse"

(lambda spash.clear [{: tab : pool : p-pool : cell-size &as self}]
  "Clear the spash.
This will remove reference to any external tables or variables."
  (each [_ tobj (ipairs pool)]
    (let [(x1 x2 y1 y2) (get-cell-range cell-size (drop tobj))
          t-pool p-pool]
      (tset self :p-pool pool)
      (tset self :pool t-pool)
      (trace "clearing tobj from region: x1:%s, x2:%s, y1:%s, y2:%s, tobj:%s"
           x1 x2 y1 y2 (match (type tobj) :table :table :userdata :userdata _ tobj))
      (apply-to-cells x1 x2 y1 y2 clear-cell tab))))

(fn within [mx my x y w h]
  (and (> mx x) (<= mx (+ x w)) (> my y) (<= my (+ y h))))

(local empty [])

(lambda spash.top-at-point [{: tab : cell-size} mx my]
  "Get the object at the highest z value in spash at point mx my."
  (let [index (xy->index (get-cell-range cell-size mx my 0 0))
        cell (or (. tab index) empty)]
    (var max-z 0) ;; z indexing starts at 1
    (var max-obj false)
    (each [_key {: x : y : w : h : z : obj} (ipairs cell)]
      ;; destructuring should not create a table
      (let [w (within mx my x y w h )]
        (trace "indexing point: mx:%s my:%s, x:%s, y:%s, w:%s, h:%s, z:%s, obj:%s w:%s"
               mx my x y w h z obj (tostring w))
        (set max-obj (and (or w nil) (or (and (> z max-z) obj) max-obj)))
        (set max-z (and (or w max-z) (or (and (> z max-z) z) max-z)))))
    max-obj))

(lambda spash.new [?cell-size]
  "Create a new spash. The default cell-size is 64"
  (let [cell-size (or ?cell-size 64)]
    (setmetatable {: cell-size :tab [] :pool [] :p-pool []} spash-mt)))

spash
