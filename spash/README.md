## spash.fnl - The Simple No Garbage Spatial Hash

The purpose of `spash` is to be a "UI first" spatial hash. 

Its key goals are:
1. To be as branchless as possible
2. Never trigger garbage collection
3. Provide convenient z-ordering 

`spash` provides a bare minimum spatial hash that is designed to be cleared and built each frame without creating memory churn. The only operations available are adding object, clearing the hash, and getting the highest object at a point.

## Example
To create a spash with `spash.new`
``` fennel
(local spash (require :spash))
(local sh (spash.new))
```

Add three rectangles to the spash using the `sh:add`. You can add a z value if you
want a specific object to rise in the z height.

In this example we add three objects with values 1 2 and 3. Object 2 is given 
 a z value of 4 (a value greater than the total number of objects being added to the spash)

```
(sh:add 1 0 0 100 100)
(sh:add 2 0 0 100 100 4)
(sh:add 3 0 0 100 100)
```

We can now index the spash using `sh:top-at-point`. This will return the object with the
greatest z value at point `mx,my`
```
(sh:top-at-point 50 50)
```

2 will be returned, as it was assigned a z value greater than any in the spash. If the z value of 2 had not been set to 4 the output above would be 3.

You can now clear the spash using `sh:clear`. This will reset the spash and let you start
building from scratch.

```
(sh:clear)
```

Trying to index `50,50` again will yield a `nil` value


## Functions
### spash.new
`(spash.new ?cell-size)`

Create a new spash will a cell-size of `?cell-size`.

* `?cell-size` an optional value used to override the default cell size (64)
* It returns a spash table that can be written to, indexed and cleared

### spash.add
`(spash.add spash obj x y w h ?z)`

Add a lua value `obj` to `spash` with a rectangular region of `x` `y` `w` `h`.

* `spash` a spash table created by `spash.new`
* `obj` Any lua value or table
* `x,y,w,h` Numbers representing the bounding rectangle of the object
* `?z` An optional value used to boost the z value of an obj

Providing a `?z` value will replace the order added based z value assigned by the spash. If you're adding 100 objects and you want one to always be on top give it a `?z` value of 101.
 
### spash.clear
`(spash.clear spash)`

Clear / reset the spash. This will remove reference to any external tables or variables.

* `spash` a spash table created by `spash.new`

### spash.top-at-point
`(spash.top-at-point spash mx my)`

Index the `spash` at `mx` `my` and return the object with the highest z value.

* `spash` a spash table created by `spash.new`
* `mx,my` a point in space represented by two numbers mx and my


## Use with Lua
`spash` is written in fennel, but can be compiled to be used without the fennel transpiler
by lua or any language that targets lua. 

A lua compiled version of the library can be found in the `/build` directory.
