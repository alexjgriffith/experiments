local spash = {version = "0.1.0"}
local spash_mt = {__index = spash}
local function xy__3eindex(x, y)
  return (x + (y * 10000000))
end
local function add_tab(pool, p_pool, obj, x, y, w, h, _3fz)
  local id = (#pool + 1)
  local tobj = (p_pool[#p_pool] or {})
  do end (tobj)["obj"] = obj
  tobj["x"] = x
  tobj["y"] = y
  tobj["w"] = w
  tobj["h"] = h
  tobj["z"] = _3fz
  tobj["id"] = id
  table.remove(p_pool, #p_pool)
  do end (pool)[id] = tobj
  return tobj
end
local function drop(tobj)
  local id = tobj.id
  tobj["obj"] = nil
  return tobj.x, tobj.y, tobj.w, tobj.h
end
local function get_cell_range(cell_size, x, y, w, h)
  local floor = math.floor
  local x1 = floor((x / cell_size))
  local x2 = floor(((x + w) / cell_size))
  local y1 = floor((y / cell_size))
  local y2 = floor(((y + h) / cell_size))
  return x1, x2, y1, y2
end
local function apply_to_cells(x1, x2, y1, y2, fun, ...)
  for i = x1, x2 do
    for j = y1, y2 do
      local index = xy__3eindex(i, j)
      fun(index, ...)
    end
  end
  return nil
end
local function insert_into_cell(index, tab, tobj)
  local cell = (tab[index] or {})
  do end (tobj)["z"] = (tobj.z or (1 + #cell))
  table.insert(cell, tobj)
  do end (tab)[index] = cell
  return nil
end
spash.add = function(_1_, obj, x, y, w, h, _3fz)
  local _arg_2_ = _1_
  local pool = _arg_2_["pool"]
  local p_pool = _arg_2_["p-pool"]
  local tab = _arg_2_["tab"]
  local cell_size = _arg_2_["cell-size"]
  _G.assert((nil ~= h), "Missing argument h on spash.fnl:61")
  _G.assert((nil ~= w), "Missing argument w on spash.fnl:61")
  _G.assert((nil ~= y), "Missing argument y on spash.fnl:61")
  _G.assert((nil ~= x), "Missing argument x on spash.fnl:61")
  _G.assert((nil ~= obj), "Missing argument obj on spash.fnl:61")
  _G.assert((nil ~= cell_size), "Missing argument cell-size on spash.fnl:61")
  _G.assert((nil ~= tab), "Missing argument tab on spash.fnl:61")
  _G.assert((nil ~= p_pool), "Missing argument p-pool on spash.fnl:61")
  _G.assert((nil ~= pool), "Missing argument pool on spash.fnl:61")
  local tobj = add_tab(pool, p_pool, obj, x, y, w, h, _3fz)
  local x1, x2, y1, y2 = get_cell_range(cell_size, x, y, w, h)
  return apply_to_cells(x1, x2, y1, y2, insert_into_cell, tab, tobj)
end
local function clear_cell(index, tab)
  local cell = tab[index]
  for key, _ in ipairs(cell) do
    cell[key] = nil
  end
  return nil
end
spash.clear = function(_3_)
  local _arg_4_ = _3_
  local tab = _arg_4_["tab"]
  local pool = _arg_4_["pool"]
  local p_pool = _arg_4_["p-pool"]
  local cell_size = _arg_4_["cell-size"]
  local self = _arg_4_
  _G.assert((nil ~= self), "Missing argument self on spash.fnl:79")
  _G.assert((nil ~= cell_size), "Missing argument cell-size on spash.fnl:79")
  _G.assert((nil ~= p_pool), "Missing argument p-pool on spash.fnl:79")
  _G.assert((nil ~= pool), "Missing argument pool on spash.fnl:79")
  _G.assert((nil ~= tab), "Missing argument tab on spash.fnl:79")
  for _, tobj in ipairs(pool) do
    local x1, x2, y1, y2 = get_cell_range(cell_size, drop(tobj))
    local t_pool = p_pool
    self["p-pool"] = pool
    self["pool"] = t_pool
    apply_to_cells(x1, x2, y1, y2, clear_cell, tab)
  end
  return nil
end
local function within(mx, my, x, y, w, h)
  return ((mx > x) and (mx <= (x + w)) and (my > y) and (my <= (y + h)))
end
local empty = {}
spash["top-at-point"] = function(_5_, mx, my)
  local _arg_6_ = _5_
  local tab = _arg_6_["tab"]
  local cell_size = _arg_6_["cell-size"]
  _G.assert((nil ~= my), "Missing argument my on spash.fnl:96")
  _G.assert((nil ~= mx), "Missing argument mx on spash.fnl:96")
  _G.assert((nil ~= cell_size), "Missing argument cell-size on spash.fnl:96")
  _G.assert((nil ~= tab), "Missing argument tab on spash.fnl:96")
  local index = xy__3eindex(get_cell_range(cell_size, mx, my, 0, 0))
  local cell = (tab[index] or empty)
  local max_z = 0
  local max_obj = false
  for _key, _7_ in ipairs(cell) do
    local _each_8_ = _7_
    local x = _each_8_["x"]
    local y = _each_8_["y"]
    local w = _each_8_["w"]
    local h = _each_8_["h"]
    local z = _each_8_["z"]
    local obj = _each_8_["obj"]
    local w0 = within(mx, my, x, y, w, h)
    max_obj = ((w0 or nil) and (((z > max_z) and obj) or max_obj))
    max_z = ((w0 or max_z) and (((z > max_z) and z) or max_z))
  end
  return max_obj
end
spash.new = function(_3fcell_size)
  local cell_size = (_3fcell_size or 64)
  return setmetatable({["cell-size"] = cell_size, tab = {}, pool = {}, ["p-pool"] = {}}, spash_mt)
end
return spash
